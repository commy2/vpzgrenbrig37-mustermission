#define VERSION 09.08.2020
#define IDC_OK 1

params ["_display"];

private _ok = _display displayCtrl IDC_OK;
ctrlPosition _ok params ["_left", "_top", "_width", "_height"];
_top = _top - 2.2 * _height;

// create ui elements
private _text = _display ctrlCreate ["RscText", -1];
_text ctrlSetPosition [_left, _top, _width, _height];
_text ctrlCommit 0;
_text ctrlSetText toUpper ("Muster " + 'VERSION');
_text ctrlSetFont "PuristaLight";
_text ctrlSetBackgroundColor [0,0,0,0.8];
