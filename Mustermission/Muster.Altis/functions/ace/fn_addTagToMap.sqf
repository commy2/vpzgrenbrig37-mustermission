["ace_tagCreated", {
	params ["_tag", "_texture", "_object", "_unit"];
	if (_object isKindOf "building") then {
		_marker = createMarker [str(_tag), position _object];
		_marker setMarkerShape "ICON";
		_marker setMarkerType "hd_dot";

		if (!(["black", _texture] call BIS_fnc_inString)) then {
			if (["red", _texture] call BIS_fnc_inString) then {
				_marker setMarkerColor "ColorRed";
			};
			if (["green", _texture] call BIS_fnc_inString) then {
				_marker setMarkerColor "ColorGreen";
			};
			if (["blue", _texture] call BIS_fnc_inString) then {
				_marker setMarkerColor "ColorBlue";
			};
		};
	};
}] call CBA_fnc_addEventhandler;