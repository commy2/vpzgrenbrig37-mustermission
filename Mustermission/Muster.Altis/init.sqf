﻿//init.sqf

/*
Syntax:		enableSaving [enable, save]
Parameters:	[enable, save]: Array
				enable: Boolean - allow manual saving
				save: Boolean - create autosave
siehe: https://community.bistudio.com/wiki/enableSaving
*/
enableSaving [false, false];

tf_give_microdagr_to_soldier = false;


// ----- Zeus -----
//Fügt alle Einheiten bei jedem vorhandenen Zeus hinzu und Synchronisiert neu gesetzte/gelöschte Einheiten InGame
if (isServer) then {
    ["PZG_AddCuratorObject", {
        params ["_object"];
        {
            _x addCuratorEditableObjects [[_object], true];
        } forEach allCurators;
    }] call CBA_fnc_addEventHandler;

    ["PZG_AddCuratorGroup", {
        params ["_group"];
        {
            ["PZG_AddCuratorObject", _x] call CBA_fnc_localEvent;
        } forEach units _group;
    }] call CBA_fnc_addEventHandler;
    {
        ["PZG_AddCuratorObject", _x] call CBA_fnc_localEvent;
    } forEach (allUnits + vehicles);
};

{
    _x addEventHandler ["CuratorObjectPlaced", {
        params ["", "_object"];
        ["PZG_AddCuratorObject", _object] call CBA_fnc_serverEvent;
        ["PZG_MoveGroupToHC", group _object] call CBA_fnc_serverEvent;
    }];

    _x addEventHandler ["CuratorGroupPlaced", {
        params ["", "_group"];
        ["PZG_AddCuratorGroup", _group] call CBA_fnc_serverEvent;
        ["PZG_MoveGroupToHC", _group] call CBA_fnc_serverEvent;
    }];
} forEach allCurators;

// kicks clients that connect without the custom map addon loaded.
if (hasInterface && !isClass (configFile >> "CfgWorlds" >> worldName)) then {
    0 spawn {
        // kick
        waitUntil {!isNull findDisplay 46};
        findDisplay 46 closeDisplay 0;
    };
};

// Rasenmäher
private _action = ["PZG_CutGrass", "Rasenmäher", "", {
    {
        ACE_player call ace_common_fnc_goKneeling;
        //private _soundSources = allMissionObjects "#soundonvehicle";
        //playSound "PZG_LawnMower";
        //PZG_LawnMowerSound = (allMissionObjects "#soundonvehicle" - _soundSources) param [0, objNull];

        ["Schneide Gras", 5, {true}, {
            private _dummy = "Land_ClutterCutter_large_F" createVehicle [0,0,0];
            _dummy setPosASL getPosASL ACE_player;
        }, {
            //deleteVehicle PZG_LawnMowerSound;
        }] call CBA_fnc_progressBar;
    } call CBA_fnc_execNextFrame;
}, {ACE_player in ACE_player}, {}, []] call ace_interact_menu_fnc_createAction;
["CAManBase", 1, ["ACE_SelfActions"], _action, true] call ace_interact_menu_fnc_addActionToClass;

// fix TFAR
#define TFAR_VOLUME_NORMAL 20

["PZG_fixScreaming", "OnSpeakVolume", {
    params ["_unit", "_volume"];

    if (_volume != TFAR_VOLUME_NORMAL) then {
        TFAR_VOLUME_NORMAL call TFAR_fnc_setVoiceVolume;
    };
}] call TFAR_fnc_addEventHandler;

// DLC spam remover
[{
    {
        private _display = _x;
        private _controls = allControls _display;

        private _index = _controls findIf {ctrlText _x == "\A3\Data_F_Orange\Logos\arma3_orange_logo_ca.paa"};

        {
            _x ctrlSetPosition [0,0,0,0];
            _x ctrlCommit 0;
        } forEach (_controls select [_index-2, 7]);
    } forEach allDisplays;
}, nil, 1] call CBA_fnc_waitAndExecute;
