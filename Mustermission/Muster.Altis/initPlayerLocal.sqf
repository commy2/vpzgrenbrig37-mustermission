﻿[] spawn {
	sleep 1; //make sure this runs second
	xx_totalKills = 0;

    ["acex_killTracker_kill", {
	xx_totalKills = xx_totalKills + 1;
	acex_killTracker_outputText = format ["Kills: %1<br/>%2", xx_totalKills, acex_killTracker_outputText];
    }] call CBA_fnc_addEventHandler;
	["acex_killTracker_death", {
	acex_killTracker_outputText = format ["Kills: %1<br/>%2", xx_totalKills, acex_killTracker_outputText];
    }] call CBA_fnc_addEventHandler;
};

params ["_unit"];

_unit addEventHandler ["killed", {
    params ["_unit"];
    if (isNull _unit) exitWith {};
    PZG_radio_settings = backpackContainer _unit getVariable "radio_settings";
}];

_unit addEventHandler ["respawn", {
    params ["_unit"];
    backpackContainer _unit setVariable ["radio_settings", PZG_radio_settings];
}];

// update insignia
private _insignia = [
    "BWA3_insignia_00_soldat",
    "BWA3_insignia_01_gefreiter",
    "BWA3_insignia_02_obergefreiter",
    "BWA3_insignia_03_hauptgefreiter",
    "BWA3_insignia_04_stabsgefreiter",
    "BWA3_insignia_05_oberstabsgefreiter",
    "BWA3_insignia_06_unteroffizier",
    "BWA3_insignia_07_stabsunteroffizier",
    "BWA3_insignia_08_feldwebel"
];

private _ranks = [
    "Panzergrenadier",
    "Gefreiter",
    "Obergefreiter",
    "Hauptgefreiter",
    "Stabsgefreiter",
    "Oberstabsgefreiter",
    "Unteroffizier",
    "Stabsunteroffizier",
    "Feldwebel"
];

private _rank = squadParams _unit param [0, []] param [5, ""];

_insignia = _insignia param [_ranks find _rank, ""];

if (_insignia != "") then {
    [_unit, _insignia] call BIS_fnc_setUnitInsignia;
};

// sheepster fix
if (getPlayerUID _unit == "76561198020754915") then {
    private _fnc_update = {
        params ["_unit", "", "_vehicle"];
        if (_vehicle isKindOf "BWA3_Puma_base") then {
            {
                private _controls = allControls (uiNamespace getVariable "RscUnitInfo") select {ctrlClassName _x == "RscPicture"};

                {
                    ctrlPosition _x params ["_left", "_top", "_width", "_height"];

                    private _scale = 0.6;
                    _width = _width * _scale;
                    _height = _height * _scale;
                    _left = 0.5-_width/2;

                    _x ctrlSetPosition [_left, _top, _width, _height];
                    _x ctrlCommit 0;
                } forEach _controls;
            } call CBA_fnc_execNextFrame;
        };
    };
    [_unit, nil, vehicle _unit] call _fnc_update;
    _unit addEventHandler ["GetInMan", _fnc_update];
};
