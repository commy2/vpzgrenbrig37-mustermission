#define IDC_OK 1
#define IDC_MAP 51
#define IDC_PZG_CB_TICKETS 1127

params ["_display"];

private _ok = _display displayCtrl IDC_OK;
ctrlPosition _ok params ["_left", "_top", "_width", "_height"];
_top = _top - 1.1 * _height;

// create ui elements
private _text = _display ctrlCreate ["RscText", -1];
_text ctrlSetPosition [_left, _top, _width, _height];
_text ctrlCommit 0;
_text ctrlSetText toUpper "Ticketmission:";
_text ctrlSetFont "PuristaLight";
_text ctrlSetBackgroundColor [0,0,0,0.8];

private _checkbox = _display ctrlCreate ["ctrlCheckbox", IDC_PZG_CB_TICKETS];
_checkbox ctrlSetPosition [_left + _width - _height / (safezoneW/safezoneH), _top, _height / (safezoneW/safezoneH), _height];
_checkbox ctrlCommit 0;

if ((isServer || {serverCommandAvailable "#logout"}) && {time == 0}) then {
    _checkbox ctrlAddEventHandler ["CheckedChanged", {
        params ["_checkbox", "_checked"];
        _checked = _checked == 1;
        missionNamespace setVariable ["PZG_isTicketMission", _checked, true];
    }];
} else {
    _checkbox ctrlEnable false;
};

// add ui functionality
if (isNil "PZG_isTicketMission") then {
    PZG_isTicketMission = false;
};

private _map = _display displayCtrl IDC_MAP;

_map ctrlAddEventHandler ["Draw", {
    params ["_map"];
    private _display = ctrlParent _map;
    private _checkbox = _display displayCtrl IDC_PZG_CB_TICKETS;

    if !(cbChecked _checkbox isEqualTo PZG_isTicketMission) then {
        _checkbox cbSetChecked PZG_isTicketMission;
    };
}];
