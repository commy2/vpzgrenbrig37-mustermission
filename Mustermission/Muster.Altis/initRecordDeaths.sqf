#define STRING "Eigene Ausfälle: %1"

if (_this == "PreInit") then {
    if (isServer) then {
        missionNamespace setVariable ["PZG_deaths_record", true call CBA_fnc_createNamespace, true];
    };

    if (hasInterface) then {
        ["CAManBase", "killed", {
            params ["_unit"];

            if (_unit == player) then {
                private _deaths = PZG_deaths_record getVariable [profileNameSteam, 0];
                _deaths = _deaths + 1;

                PZG_deaths_record setVariable [profileNameSteam, _deaths, true];
                PZG_deaths = format [STRING, _deaths];
            };
        }] call CBA_fnc_addClassEventHandler;
    };
};

if (_this == "PostInit") then {
    private _deaths = PZG_deaths_record getVariable [profileNameSteam, 0];
    PZG_deaths = format [STRING, _deaths];
};
