#include "_CAMO.hpp"

if (hasInterface) then {
    // people
    ["CAManBase", "InitPost", {
        params ["_unit"];
        if (_unit != player) exitWith {};

        private _name = _unit getVariable "PZG_Loadout";

        #ifndef TROPEN
        private _camo = "FLECK";
        #else
        private _camo = "TROPEN";
        #endif
        #ifdef RETRO
        _camo = "RETRO";
        #endif
        #ifdef RETRO_WINTER
        _camo = "RETRO_WINTER";
        #endif

        if (!isNil "_name") then {
            _unit setUnitLoadout call compile loadFile format ["loadouts\%1\%2.sqf", _camo, _name];
        };
    }] call CBA_fnc_addClassEventHandler;
};

if (isServer) then {
    // vehicles
    #define TYPES ["BWA3_Eagle_Fleck", "BWA3_Puma_Fleck", "BWA3_Leopard2_Fleck", "BWA3_Container_Fleck"]

    #ifdef TROPEN
    private _fnc_paintTropen = {
        params ["_vehicle"];

        private _classname = [toUpper typeOf _vehicle, "_FLECK", "_TROPEN"] call CBA_fnc_replace;
        private _config = configFile >> "CfgVehicles" >> _classname;
        private _textureSource = selectRandomWeighted getArray (_config >> "textureList");

        private _textureConfig = _config >> "TextureSources" >> _textureSource;
        private _textures = getArray (_textureConfig >> "textures");
        private _materials = getArray (_textureConfig >> "materials");

        {
            _vehicle setObjectTextureGlobal [_forEachIndex, _x];
        } forEach _textures;

        {
            _vehicle setObjectMaterialGlobal [_forEachIndex, _x];
        } forEach _materials;
    };

    {
        [_x, "InitPost", _fnc_paintTropen] call CBA_fnc_addClassEventHandler;
    } forEach TYPES;
    #endif

    // ammo boxes
    #define TYPES [\
        "ACE_Box_Misc", "ACE_medicalSupplyCrate_advanced", "Box_NATO_Grenades_F",\
        "BWA3_Box_Weapons", "BWA3_Box_Launchers", "BWA3_Box_Ammo",\
        "BWA3_Box_Support", "BWA3_Box_Gear_Fleck",\
        "BWA3_Leopard2_Fleck", "BWA3_Puma_Fleck", "Redd_Marder_1A5_Flecktarn",\
        "TFAR_NATO_Radio_Crate"\
    ]

    {
        [_x, "InitPost", {
            params ["_container"];
            private _name = typeOf _container;

            #ifndef TROPEN
            private _camo = "FLECK";
            #else
            private _camo = "TROPEN";
            #endif
            #ifdef RETRO
            _camo = "RETRO";
            #endif
            #ifdef RETRO_WINTER
            _camo = "RETRO_WINTER";
            #endif

            private _cargo = call compile loadFile format ["cargo\%1\%2.sqf", _camo, _name];

            clearWeaponCargoGlobal _container;
            {
                //_container addWeaponCargoGlobal [_x, 1];
                [{time > 0}, {(_this#0) addWeaponCargoGlobal (_this#1)}, [_container, [_x, 1]]] call CBA_fnc_waitUntilAndExecute;
            } forEach (_cargo#0);

            clearMagazineCargoGlobal _container;
            {
                _container addMagazineCargoGlobal [_x, 1];
            } forEach (_cargo#1);

            clearItemCargoGlobal _container;
            {
                _container addItemCargoGlobal [_x, 1];
            } forEach (_cargo#2);

            clearBackpackCargoGlobal _container;
            {
                _container addBackpackCargoGlobal [_x, 1];
            } forEach (_cargo#3);
        }, false] call CBA_fnc_addClassEventHandler;
    } forEach TYPES;

    #ifdef RETRO
    #define RETRO_SPZ
    #endif

    #ifdef RETRO_WINTER
    #define RETRO_SPZ
    #endif

    #ifdef RETRO_SPZ
    ["BWA3_Puma_base", "InitPost", {
        params ["_vehicle"];
        private _pos = getPosASL _vehicle;
        private _dir = getDir _vehicle;
        deleteVehicle _vehicle;

        _vehicle = "Redd_Marder_1A5_Flecktarn" createVehicle [0,0,0];
        _vehicle setPosASL _pos;
        _vehicle setDir _dir;

        private _helper = "#particlesource" createVehicleLocal [0,0,0];
        _helper setDir _dir;
        _vehicle attachTo [_helper];
        _vehicle setDir 0;

        _helper spawn {sleep 10; deleteVehicle _this};
    }, nil, nil, true] call CBA_fnc_addClassEventHandler;
    #endif
};
