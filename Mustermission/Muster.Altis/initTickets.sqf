#define TICKETS_PER_GROUP 4

if (isNil "PZG_tickets_results") then {
    PZG_tickets_results = "";
};

if (isServer) then {
    ["PZG_PlayerKilled", {
        params ["_unit"];
        private _group = group _unit;

        if (missionNamespace getVariable ["PZG_isTicketMission", false]) then {
            // HANDLE TICKET MISSIONS
            if (side _group != west) then {
                1e7 remoteExec ["setPlayerRespawnTime", _unit];
            } else {
                private _tickets = _group getVariable ["PZG_Tickets", TICKETS_PER_GROUP];
                _tickets = _tickets - 1;

                if (_tickets == 0) then {
                    [["Der nächste Tod ist endgültig.", "PLAIN", 5, true]] remoteExec ["cutText", _group];
                };

                if (_tickets < 0) then {
                    1e7 remoteExec ["setPlayerRespawnTime", _unit];
                };

                _group setVariable ["PZG_Tickets", _tickets];
            };

            // update debriefing score screen
            private _groups = allGroups select {side _x == west && {units _x findIf {isPlayer _x} != -1}};
            private _results = "    Verbleibende Tickets<br/><br/>";
            {
                _results = _results + format ["%1:    %2<br/>", groupId _x, _x getVariable ["PZG_tickets", TICKETS_PER_GROUP]];
            } forEach _groups;

            missionNamespace setVariable ["PZG_tickets_results", _results, true];
        } else {
            // HANDLE NON-TICKET MISSIONS
            if (side _group == west) then {
                private _deaths = _group getVariable ["PZG_Deaths", 0];
                _deaths = _deaths + 1;

                _group setVariable ["PZG_Deaths", _deaths];
            };

            // update debriefing score screen
            private _groups = allGroups select {side _x == west && {units _x findIf {isPlayer _x} != -1}};
            private _results = "    Ausfälle<br/><br/>";
            {
                _results = _results + format ["%1:    %2<br/>", groupId _x, _x getVariable ["PZG_Deaths", 0]];
            } forEach _groups;

            missionNamespace setVariable ["PZG_tickets_results", _results, true];
        };
    }] call CBA_fnc_addEventHandler;
};

if (hasInterface) then {
    ["CAManBase", "killed", {
        params ["_unit"];

        if (_unit == player) then {
            ["PZG_PlayerKilled", _this] call CBA_fnc_serverEvent;
        };
    }] call CBA_fnc_addClassEventHandler;
};
