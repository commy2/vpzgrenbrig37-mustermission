// ACE Advanced Ballistics
ace_advanced_ballistics_ammoTemperatureEnabled = true;
ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
ace_advanced_ballistics_bulletTraceEnabled = true;
ace_advanced_ballistics_enabled = false;
ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue
force ace_advanced_fatigue_enabled = true;
force ace_advanced_fatigue_enableStaminaBar = true;
force ace_advanced_fatigue_loadFactor = 1;
force ace_advanced_fatigue_performanceFactor = 1.47802;
force ace_advanced_fatigue_recoveryFactor = 1.96078;
force ace_advanced_fatigue_swayFactor = 0.6;
force ace_advanced_fatigue_terrainGradientFactor = 1;

// ACE Advanced Throwing
force ace_advanced_throwing_enabled = true;
ace_advanced_throwing_enablePickUp = true;
ace_advanced_throwing_enablePickUpAttached = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Arsenal
ace_arsenal_allowDefaultLoadouts = true;
ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
ace_arsenal_fontHeight = 4.5;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 2;
force ace_captives_requireSurrenderAi = false;

// ACE Common
ace_common_allowFadeMusic = true;
force ace_common_checkPBOsAction = 0;
force ace_common_checkPBOsCheckAll = true;
force ace_common_checkPBOsWhitelist = "['armazeuscache','cntr','diwako_dui_main','diwako_dui_radar','diwako_dui_indicators','diwako_dui_buddy','pzg_modulesmokehowitzer','STGI','ST_STHud','ST_STHud_Usermenu','stui_autobrightness','stui_core','stui_grouphud','stui_groupindicators','stui_nametags','stui_stamina','bcombat','BWA3_EnablePIPOptics','rtc','ocap','ares','achilles_data_f_ares','achilles_language_f','achilles_data_f_achilles','achilles_functions_f_ares','achilles_functions_f_achilles','achilles_modules_f_ares','achilles_settings_f','achilles_ui_f','achilles_modules_f_achilles','achilleseffects']";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;
ace_noradio_enabled = true;
ace_parachute_hideAltimeter = true;

// ACE Cook off
force ace_cookoff_ammoCookoffDuration = 0;
force ace_cookoff_enable = false;
force ace_cookoff_enableAmmobox = false;
force ace_cookoff_enableAmmoCookoff = false;
force ace_cookoff_probabilityCoef = 0;

// ACE Explosives
force ace_explosives_explodeOnDefuse = false;
force ace_explosives_punishNonSpecialists = true;
force ace_explosives_requireSpecialist = false;

// ACE Fragmentation Simulation
ace_frag_enabled = true;
ace_frag_maxTrack = 10;
ace_frag_maxTrackPerFrame = 10;
ace_frag_reflectionsEnabled = false;
force ace_frag_spallEnabled = false;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showInThirdPerson = false;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = false;
force ace_hearing_disableEarRinging = true;
force ace_hearing_earplugsVolume = 0.5;
force ace_hearing_enableCombatDeafness = false;
force ace_hearing_enabledForZeusUnits = false;
force ace_hearing_unconsciousnessVolume = 0.4;

// ACE Interaction
force ace_interaction_disableNegativeRating = true;
force ace_interaction_enableMagazinePassing = true;
force ace_interaction_enableTeamManagement = true;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
ace_interact_menu_addBuildingActions = false;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = false;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = false;

// ACE Logistics
ace_cargo_enable = true;
ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_level = 0;
force ace_rearm_supply = 0;
ace_refuel_hoseLength = 12;
ace_refuel_rate = 1;
ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = true;
ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 1;
ace_repair_engineerSetting_repair = 1;
ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 0;
ace_repair_repairDamageThreshold = 0.6;
ace_repair_repairDamageThreshold_engineer = 0.4;
ace_repair_wheelRepairRequiredItems = 0;

// ACE Magazine Repack
ace_magazinerepack_timePerAmmo = 1.5;
ace_magazinerepack_timePerBeltLink = 8;
ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force ace_map_BFT_Enabled = true;
force ace_map_BFT_HideAiGroups = true;
force ace_map_BFT_Interval = 30;
force ace_map_BFT_ShowPlayerNames = false;
force ace_map_DefaultChannel = 1;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = false;
force ace_map_mapShowCursorCoordinates = false;
ace_markers_moveRestriction = 0;

// ACE Map Gestures
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
ace_map_gestures_enabled = true;
ace_map_gestures_interval = 0.03;
ace_map_gestures_maxRange = 7;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force ace_medical_ai_enabledFor = 0;
force ace_medical_AIDamageThreshold = 0.9;
force ace_medical_bleedingCoefficient = 0.25;
force ace_medical_blood_bloodLifetime = 900;
force ace_medical_blood_enabledFor = 2;
force ace_medical_blood_maxBloodObjects = 500;
force ace_medical_fatalDamageSource = 1;
force ace_medical_feedback_painEffectType = 1;
force ace_medical_fractures = 2;
force ace_medical_gui_enableActions = 0;
force ace_medical_gui_enableMedicalMenu = 1;
force ace_medical_gui_enableSelfActions = true;
force ace_medical_gui_maxDistance = 3;
force ace_medical_gui_openAfterTreatment = true;
force ace_medical_ivFlowRate = 4;
force ace_medical_limping = 2;
force ace_medical_painCoefficient = 0.5;
force ace_medical_playerDamageThreshold = 4;
force ace_medical_spontaneousWakeUpChance = 0.3;
force ace_medical_spontaneousWakeUpEpinephrineBoost = 5;
force ace_medical_statemachine_AIUnconsciousness = false;
force ace_medical_statemachine_cardiacArrestTime = 900;
force ace_medical_statemachine_fatalInjuriesAI = 0;
force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force ace_medical_treatment_advancedBandages = true;
force ace_medical_treatment_advancedDiagnose = true;
force ace_medical_treatment_advancedMedication = true;
force ace_medical_treatment_allowLitterCreation = true;
force ace_medical_treatment_allowSelfIV = 1;
force ace_medical_treatment_allowSelfStitch = 0;
force ace_medical_treatment_allowSharedEquipment = 1;
force ace_medical_treatment_clearTraumaAfterBandage = false;
force ace_medical_treatment_consumePAK = 0;
force ace_medical_treatment_consumeSurgicalKit = 0;
force ace_medical_treatment_convertItems = 0;
force ace_medical_treatment_cprSuccessChance = 0.4;
force ace_medical_treatment_holsterRequired = 0;
force ace_medical_treatment_litterCleanupDelay = 600;
force ace_medical_treatment_locationEpinephrine = 0;
force ace_medical_treatment_locationPAK = 0;
force ace_medical_treatment_locationsBoostTraining = false;
force ace_medical_treatment_locationSurgicalKit = 0;
force ace_medical_treatment_maxLitterObjects = 500;
force ace_medical_treatment_medicEpinephrine = 0;
force ace_medical_treatment_medicPAK = 1;
force ace_medical_treatment_medicSurgicalKit = 1;
force ace_medical_treatment_timeCoefficientPAK = 1;
force ace_medical_treatment_woundReopening = false;

// ACE Mk6 Mortar
ace_mk6mortar_airResistanceEnabled = false;
ace_mk6mortar_allowCompass = true;
ace_mk6mortar_allowComputerRangefinder = true;
ace_mk6mortar_useAmmoHandling = false;

// ACE Name Tags
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_playerNamesMaxAlpha = 0.8;
ace_nametags_playerNamesViewDistance = 5;
ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 1;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
force ace_nightvision_aimDownSightsBlur = 0.3;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 0.25;
force ace_nightvision_fogScaling = 0.25;
force ace_nightvision_noiseScaling = 0.4;
ace_nightvision_shutterEffects = true;

// ACE Overheating
ace_overheating_displayTextOnJam = true;
ace_overheating_enabled = true;
ace_overheating_overheatingDispersion = true;
ace_overheating_showParticleEffects = true;
ace_overheating_showParticleEffectsForEveryone = false;
ace_overheating_unJamFailChance = 0.1;
force ace_overheating_unJamOnreload = true;

// ACE Pointing
force ace_finger_enabled = true;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 4;

// ACE Pylons
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
ace_pylons_rearmNewPylons = false;
ace_pylons_requireEngineer = false;
ace_pylons_requireToolkit = true;
ace_pylons_searchDistance = 15;
ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force ace_quickmount_distance = 5;
force ace_quickmount_enabled = true;
ace_quickmount_priority = 0;
force ace_quickmount_speed = 11;

// ACE Respawn
ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = true;

// ACE Scopes
ace_scopes_correctZeroing = true;
ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
ace_scopes_defaultZeroRange = 100;
ace_scopes_enabled = true;
ace_scopes_forceUseOfAdjustmentTurrets = false;
ace_scopes_overwriteZeroRange = false;
ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
ace_scopes_zeroReferenceBarometricPressure = 1013.25;
ace_scopes_zeroReferenceHumidity = 0;
ace_scopes_zeroReferenceTemperature = 15;

// ACE Spectator
ace_spectator_enableAI = false;
ace_spectator_restrictModes = 0;
ace_spectator_restrictVisions = 0;

// ACE Switch Units
ace_switchunits_enableSafeZone = true;
ace_switchunits_enableSwitchUnits = false;
ace_switchunits_safeZoneRadius = 100;
ace_switchunits_switchToCivilian = false;
ace_switchunits_switchToEast = false;
ace_switchunits_switchToIndependent = false;
ace_switchunits_switchToWest = false;

// ACE Tagging
ace_tagging_quickTag = 1;

// ACE Uncategorized
ace_gforces_enabledFor = 1;
ace_hitreactions_minDamageToTrigger = 0.1;
ace_inventory_inventoryDisplaySize = 0;
ace_laser_dispersionCount = 2;
ace_microdagr_mapDataAvailable = 2;
ace_optionsmenu_showNewsOnMainMenu = true;
ace_overpressure_distanceCoefficient = 1;

// ACE User Interface
ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
ace_ui_firingMode = true;
ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
ace_vehiclelock_defaultLockpickStrength = 10;
force ace_vehiclelock_lockVehicleInventory = true;
ace_vehiclelock_vehicleStartingLockState = -1;

// ACE View Distance Limiter
force ace_viewdistance_enabled = true;
force ace_viewdistance_limitViewDistance = 3000;
ace_viewdistance_objectViewDistanceCoeff = 0;
force ace_viewdistance_viewDistanceAirVehicle = 6;
force ace_viewdistance_viewDistanceLandVehicle = 6;
force ace_viewdistance_viewDistanceOnFoot = 6;

// ACE Weapons
force ace_common_persistentLaserEnabled = true;
force ace_laserpointer_enabled = true;
force ace_reload_displayText = true;
ace_weaponselect_displayText = true;

// ACE Weather
ace_weather_enabled = true;
ace_weather_updateInterval = 60;
force ace_weather_windSimulation = false;

// ACE Wind Deflection
ace_winddeflection_enabled = true;
ace_winddeflection_simulationInterval = 0.05;
ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
ace_zeus_autoAddObjects = false;
ace_zeus_radioOrdnance = false;
ace_zeus_remoteWind = false;
ace_zeus_revealMines = 0;
ace_zeus_zeusAscension = false;
ace_zeus_zeusBird = false;

// ACEX Fortify
force acex_fortify_settingHint = 1;

// ACEX Headless
acex_headless_delay = 15;
acex_headless_enabled = false;
acex_headless_endMission = 0;
acex_headless_log = false;

// ACEX Sitting
force acex_sitting_enable = true;

// ACEX View Restriction
acex_viewrestriction_mode = 0;
acex_viewrestriction_modeSelectiveAir = 0;
acex_viewrestriction_modeSelectiveFoot = 0;
acex_viewrestriction_modeSelectiveLand = 0;
acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACEX Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// Bundeswehr
BWA3_Leopard_ClocknumbersDir_Commander = false;
BWA3_NaviPad_showMembers = true;
BWA3_Puma_ClocknumbersDir_Commander = false;
BWA3_Puma_ClocknumbersDir_Gunner = false;
BWA3_usePipOptics = true;

// NIArms
niarms_magSwitch = true;

// STUI Settings
STHud_Settings_ColourBlindMode = "Normal";
STHud_Settings_Font = "PuristaSemibold";
STHud_Settings_HUDMode = 3;
STHud_Settings_Occlusion = true;
force STHud_Settings_RemoveDeadViaProximity = false;
STHud_Settings_SquadBar = false;
STHud_Settings_TextShadow = 1;

// TFAR - clientside settings
TFAR_default_radioVolume = 7;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.3;
TFAR_moveWhileTabbedOut = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;

// TFAR - global settings
force TFAR_AICanHearPlayer = true;
force TFAR_AICanHearSpeaker = true;
force TFAR_enableIntercom = false;
TFAR_fullDuplex = true;
force TFAR_giveLongRangeRadioToGroupLeaders = false;
force TFAR_giveMicroDagrToSoldier = false;
force TFAR_givePersonalRadioToRegularSoldier = true;
TFAR_globalRadioRangeCoef = 1;
TFAR_instantiate_instantiateAtBriefing = false;
TFAR_objectInterceptionEnabled = true;
tfar_radiocode_east = "_opfor";
tfar_radiocode_independent = "_independent";
tfar_radiocode_west = "_bluefor";
tfar_radioCodesDisabled = false;
force TFAR_SameLRFrequenciesForSide = true;
force TFAR_SameSRFrequenciesForSide = true;
TFAR_setting_defaultFrequencies_lr_east = "";
TFAR_setting_defaultFrequencies_lr_independent = "";
force TFAR_setting_defaultFrequencies_lr_west = "40,45,76.5,70.8,65.1,60.3";
TFAR_setting_defaultFrequencies_sr_east = "";
TFAR_setting_defaultFrequencies_sr_independent = "";
force TFAR_setting_defaultFrequencies_sr_west = "40,45,76.5,70.8,65.1,60.3";
TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
TFAR_setting_DefaultRadio_Personal_east = "TFAR_pnr1000a";
TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc154";
force TFAR_setting_DefaultRadio_Personal_West = "TFAR_rt1523g_big_bwmod";
TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
force TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_anprc152";
TFAR_spectatorCanHearEnemyUnits = true;
TFAR_spectatorCanHearFriendlies = true;
TFAR_takingRadio = 2;
force TFAR_Teamspeak_Channel_Name = "Intern";
TFAR_Teamspeak_Channel_Password = "123";
tfar_terrain_interception_coefficient = 7;

// DUI - Squad Radar - Indicators
force diwako_dui_indicators_show = false;
